import { FormEvent, useEffect, useRef } from "react";
import { Button, Pagination, TextField } from "@mui/material";

import styles from "./styles/MovieSearchPage.module.scss";
import { useQuery } from "@tanstack/react-query";
import { getMovies } from "../util/httpRequests";
import MovieItem from "../components/movieSearch/MovieItem";
import SearchIcon from "@mui/icons-material/Search";
import { useAppDispatch, useAppSelector } from "../store/hooks/reduxHook";
import { searchMovieActions } from "../store/slices/searchMovieSlice";

/**
 * Page for searching movies by name
 * @returns
 */
const SearchMoviePage = () => {
  const searchElement = useRef<HTMLInputElement | null>(null);
  const selectedPage = useAppSelector((state) => state.searchMovie.actualPage);
  const dispatch = useAppDispatch();
  /**
   * Search term from input saved in redux store
   */
  const searchTerm = useAppSelector((state) => state.searchMovie.searchTerm);

  /**
   * Load movies when search term is submitted or selected page change
   */
  const { data, isError, error } = useQuery({
    queryKey: ["movies", searchTerm, selectedPage],
    queryFn: () => getMovies(searchTerm, selectedPage),
  });
  const moviesLength: number = data?.Search?.length ?? 0;

  /**
   * On submit search form save this search term to redux store
   * @param event
   */
  const handleSubmitSearch = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    dispatch(
      searchMovieActions.setSearchTerm(searchElement.current?.value ?? "")
    );
  };

  /**
   * Get count of pages will be displayed
   * @returns
   */
  const getCountOfPages = () => {
    const moviesCount = parseInt(data?.totalResults as string, 10) || 0;
    return Math.ceil(moviesCount / 10);
  };

  /**
   * Set search term if it exists in redux store
   */
  useEffect(() => {
    if (searchElement.current && searchTerm !== searchElement.current.value) {
      searchElement.current.value = searchTerm;
    }
  }, [searchTerm]);

  if (isError) {
    throw new Error(error.message);
  }

  return (
    <section className={styles["movies"]}>
      <form
        className={styles["movies__form"]}
        onSubmit={(e) => handleSubmitSearch(e)}
      >
        <TextField
          className={styles["movies__input"]}
          size="small"
          label="Movie name"
          variant="filled"
          inputRef={searchElement}
          autoFocus
        />
        <Button
          className={styles["movies__submit-button"]}
          variant="contained"
          type="submit"
          startIcon={<SearchIcon fontSize="large" />}
        >
          Search
        </Button>
      </form>

      <div className={styles["movies__wrapper"]}>
        {data?.Search?.map((movie) => (
          <MovieItem movie={movie} key={movie.imdbID} />
        ))}
      </div>

      {moviesLength > 0 && (
        <Pagination
          className={styles["movies__pagination"]}
          count={getCountOfPages()}
          page={selectedPage}
          onChange={(_e, value) =>
            dispatch(searchMovieActions.setActualPage(value))
          }
          showFirstButton
          showLastButton
        />
      )}
    </section>
  );
};

export default SearchMoviePage;
