import { PayloadAction, createSlice } from "@reduxjs/toolkit";

interface SearchMovieState {
  actualPage: number;
  searchTerm: string;
}

const initialState: SearchMovieState = {
  actualPage: 1,
  searchTerm: "",
};

/**
 * Slice for saving search term and selected page
 */
const searchMovieSlice = createSlice({
  name: "movieSearch",
  initialState,
  reducers: {
    setActualPage: (state, action: PayloadAction<number>) => {
      state.actualPage = action.payload;
    },
    setSearchTerm: (state, action: PayloadAction<string>) => {
      state.searchTerm = action.payload;
    },
  },
});

export const searchMovieActions = searchMovieSlice.actions;
export default searchMovieSlice.reducer;
