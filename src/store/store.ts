import { configureStore } from "@reduxjs/toolkit";
import searchMovieReducer from "./slices/searchMovieSlice";
import favoriteMoviesReducer from "./slices/favoriteMoviesSlice";

const store = configureStore({
  reducer: {
    searchMovie: searchMovieReducer,
    favoriteMovies: favoriteMoviesReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
