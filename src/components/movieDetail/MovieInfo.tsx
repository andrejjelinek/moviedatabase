import { IconButton } from "@mui/material";
import { MovieDetail } from "../../models/movies/MovieDetail";
import styles from "./styles/MovieInfo.module.scss";
import DoNotDisturbAltIcon from "@mui/icons-material/DoNotDisturbAlt";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import StarIcon from "@mui/icons-material/Star";
import CloseIcon from "@mui/icons-material/Close";
import MoviePlotAndRating from "./MoviePlotAndRating";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../store/hooks/reduxHook";
import { favoriteMoviesActions } from "../../store/slices/favoriteMoviesSlice";
import { useEffect, useState } from "react";

interface IProps {
  movie: MovieDetail | null;
}

/**
 * Component for displaying basic information about movie and adding it to the favorites
 * @param props
 * @returns
 */
const MovieInfo: React.FC<IProps> = (props) => {
  const { movie } = props;
  const [isAddedToFavorites, setIsAddedToFavorites] = useState<boolean>(false);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  /**
   * Get list of favorite movies from redux store
   */
  const favoriteMovies = useAppSelector(
    (state) => state.favoriteMovies.favoriteMovies
  );

  /**
   * Navigate back to main page
   */
  const handleNavigateBack = () => {
    navigate(-1);
  };

  /**
   * Add movie to favorites list
   */
  const handleAddMovieToFavorites = () => {
    if (movie) {
      setIsAddedToFavorites(true);
      dispatch(favoriteMoviesActions.addMovieToFavorites(movie.imdbID));
    }
  };

  /**
   * Check if movie is added to favorites list
   */
  useEffect(() => {
    const favoriteMovie = favoriteMovies.find(
      (movieId) => movieId === movie?.imdbID
    );

    if (favoriteMovie) {
      setIsAddedToFavorites(true);
    }
  }, [favoriteMovies, movie]);

  return (
    <section className={styles["movie"]}>
      <IconButton
        className={styles["movie__close-icon"]}
        onClick={handleNavigateBack}
      >
        <CloseIcon />
      </IconButton>

      <div className={styles["movie-info"]}>
        {movie?.Poster != "N/A" ? (
          <div className={styles["movie-info__image"]}>
            <img
              srcSet={`${movie?.Poster}?w=248&fit=crop&auto=format&dpr=2 2x`}
              src={`${movie?.Poster}?w=248&fit=crop&auto=format`}
              alt={movie?.Title}
              loading="lazy"
            />
          </div>
        ) : (
          <div className={styles["movie-info__no-image"]}>
            <DoNotDisturbAltIcon />
            No image
          </div>
        )}

        <div className={styles["movie-info__details"]}>
          <div className={styles["movie-info__title"]}>
            <p>
              {movie?.Title}
              {isAddedToFavorites ? (
                <StarIcon fontSize="large" />
              ) : (
                <IconButton onClick={handleAddMovieToFavorites}>
                  <StarBorderIcon fontSize="large" color="info" />
                </IconButton>
              )}
            </p>

            <p>
              <span>{movie?.Type}</span>/ {movie?.Genre}
            </p>
            <p>
              {movie?.Country}, {movie?.Year}, {movie?.Runtime}
            </p>
          </div>

          <p className={styles["movie-info__details--text"]}>
            <span>Director:</span>
            {movie?.Director}
          </p>
          <p className={styles["movie-info__details--text"]}>
            <span>Writer:</span>
            {movie?.Writer}
          </p>
          <p className={styles["movie-info__details--text"]}>
            <span>Actors:</span>
            {movie?.Actors}
          </p>
          <p className={styles["movie-info__details--text"]}>
            <span>Language:</span>
            {movie?.Language}
          </p>
          <p className={styles["movie-info__details--text"]}>
            <span>Production:</span>
            {movie?.Production}
          </p>
          <p className={styles["movie-info__details--text"]}>
            <span>DVD:</span>
            {movie?.DVD}
          </p>
          <p className={styles["movie-info__details--text"]}>
            <span>Released:</span>
            {movie?.Released}
          </p>
          <p className={styles["movie-info__details--text"]}>
            <span>Web:</span>
            {movie?.Website != "N/A" ? (
              <a href={movie?.Website} />
            ) : (
              movie?.Website
            )}
          </p>
          <p className={styles["movie-info__details--text"]}>
            <span>Awards:</span>
            {movie?.Awards}
          </p>
        </div>
      </div>
      <MoviePlotAndRating movie={movie} />
    </section>
  );
};

export default MovieInfo;
