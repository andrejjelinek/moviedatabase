import Header from "../components/global/Header";
import { Outlet } from "react-router-dom";

/**
 * Component for displaying RootPage - Header and other selected page
 * @returns
 */
const RootPage = () => {
  return (
    <>
      <Header />
      <Outlet />
    </>
  );
};

export default RootPage;
