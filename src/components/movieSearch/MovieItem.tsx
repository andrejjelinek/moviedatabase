import React from "react";
import { Search } from "../../models/movies/MovieSearch";
import { Tooltip } from "@mui/material";
import { useNavigate } from "react-router-dom";
import DoNotDisturbAltIcon from "@mui/icons-material/DoNotDisturbAlt";
import styles from "./styles/MovieItem.module.scss";

interface IProps {
  movie: Search;
}

/**
 * Component that represents movie from movies search result
 * @param props
 * @returns
 */
const MovieItem: React.FC<IProps> = (props) => {
  const { movie } = props;
  const navigate = useNavigate();

  /**
   * Navigate to detail movie page
   */
  const handleNavigateToDetail = () => {
    navigate(`detail/${movie.imdbID}`);
  };

  return (
    <div className={`${styles["movie-item"]}`}>
      {movie.Poster != "N/A" ? (
        <div
          className={styles["movie-item__image"]}
          onClick={handleNavigateToDetail}
        >
          <img
            srcSet={`${movie.Poster}?w=248&fit=crop&auto=format&dpr=2 2x`}
            src={`${movie.Poster}?w=248&fit=crop&auto=format`}
            alt={movie.Title}
            loading="lazy"
          />
        </div>
      ) : (
        <div
          className={styles["movie-item__no-image"]}
          onClick={handleNavigateToDetail}
        >
          <DoNotDisturbAltIcon />
          No image
        </div>
      )}
      <div className={styles["movie-item__info"]}>
        {movie.Title.length > 60 ? (
          <Tooltip
            title={
              <p className={styles["movie-item__info--tooltip-text"]}>
                {movie.Title}
              </p>
            }
          >
            <p>{movie.Title.slice(0, 60)}...</p>
          </Tooltip>
        ) : (
          <p>{movie.Title}</p>
        )}
        <div className={styles["movie-item__detail"]}>
          <div className={styles["movie-item__type"]}>
            <span>Type:</span>
            <span>{movie.Type}</span>
          </div>
          <span>{movie.Year}</span>
        </div>
      </div>
    </div>
  );
};

export default MovieItem;
