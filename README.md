# Movie Database app

# React + TypeScript + Vite

# Table of contents

- [Project description](#ProjectDescription)
- [Project structure](#ProjectStructure)
- [Installation](#Installation)
- [Used packages](#UsedPackages)
- [Link to test](#TestLink)

# ProjectDescription

This simple app demonstrate movie database where we can find movies by name, displaying information about movie and add them to favorite list of movies.
Web app contains three pages:

- Movies - list of movies which are displayed after submit search input. On click to movie is open new page with information about movie.
- Details - all information about selected movies. User can add this movie to favorites.
- Favorite movies - list of favorite movies, user can remove this movie or open detail info page.

All movies and informations we get from api https://www.omdbapi.com/ with axios.
This data fetching is managed by react-query.
Page navigation is created with react-router-dom.
Redux-toolkit is used for saving inputed search term for finding movie and actual selected page, so when user click on movie detail and then return back search term and actual page are same as before. Also for managing favorite movies is used redux. Favorite movies IDs are saved in client browser local storage. When page is loaded array of this IDs is loaded and savevd with redux. When user click on favorite movies page firstly are sended requests for loading movies details for each saved movie and then this movies are displayed in table where we can remove them or click to show movie details.
Some components are from Material UI library and others are manually styled with scss.

# ProjectStructure

src/assets - icons

src/components - all components for pages

src/components/favofiteMovies - components with scss styles for favorite movies

src/components/movieDetail - components with scss styles for movie information

src/components/movieSearch - components with scss styles for searching movies

src/components/global - global components with scss styles (header)

src/models - interfaces for REST APi data

src/pages - pages for routing (searchMovie, detailMovie, favofiteMovies, errorPage)

src/pages/styles - scss files for styling pages

src/scss - base css, scss setting (colors, responsivity breakpoints)

src/store - configuration for redux toolkit

src/store/hooks - export dispatcht method for calling method and selector for getting data from redux

src/store/slices - created slices for searching state management and favorite movies state management

src/util - file with axiox http request methods

# Installation

- npm install
- npm run dev

# UsedPackages

- react-toastify

  - https://www.npmjs.com/package/react-toastify
  - error/success notifications

- react-router-dom

  - https://reactrouter.com/en/main
  - page navigation

- react-redux with redux-toolkit

  - https://redux-toolkit.js.org/introduction/getting-started
  - state and data management

- react-query

  - https://tanstack.com/query/v3/docs/react/overview
  - managing and caching asynchronous data

- axios

  - https://axios-http.com/docs/intro
  - HTTP client

- material UI

  - https://mui.com/
  - UI component library

  # TestLink

  https://movie-database-c4a3.onrender.com/
