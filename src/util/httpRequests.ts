import { QueryClient } from "@tanstack/react-query";
import axios from "axios";
import { MovieSearch } from "../models/movies/MovieSearch";
import { MovieDetail } from "../models/movies/MovieDetail";
import { toast } from "react-toastify";

export const queryClient = new QueryClient();
const url = `https://omdbapi.com/?apikey=739ed910`;
/**
 * Send request to get movies by searchTerm and selected page
 * @param searchTerm - movie name
 * @param selectedPage - selected page
 * @returns MovieSearch object or NULL
 */
export const getMovies = async (
  searchTerm: string,
  selectedPage: number
): Promise<MovieSearch | null> => {
  if (searchTerm === "") {
    return null;
  }

  const result = await axios.get(`${url}&s=${searchTerm}&page=${selectedPage}`);

  if (result.status !== 200) {
    throw new Error(result.statusText);
  } else if (result.data.Response !== "True") {
    toast.error(result.data.Error);
  }
  return result.data;
};

/**
 * Send request to get movie information
 * @param movieId - string id of the movie
 * @returns MovieDetail object or NULL
 */
export const getMovieDetailInfo = async (
  movieId: string
): Promise<MovieDetail | null> => {
  if (movieId === "") {
    return null;
  }

  const result = await axios.get(`${url}&i=${movieId}`);

  if (result.status !== 200) {
    throw new Error(result.statusText);
  }

  return result.data;
};
