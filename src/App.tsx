import "./main.scss";
import "./App.css";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import SearchMoviePage from "./pages/SearchMoviePage";
import RootPage from "./pages/RootPage";
import DetailMoviePage from "./pages/DetailMoviePage";
import { QueryClientProvider } from "@tanstack/react-query";
import { queryClient } from "./util/httpRequests";
import FavoriteMoviesPage from "./pages/FavoriteMoviesPage";
import { useEffect } from "react";
import { useAppDispatch } from "./store/hooks/reduxHook";
import { favoriteMoviesActions } from "./store/slices/favoriteMoviesSlice";
import ErrorPage from "./pages/ErrorPage";

function App() {
  const dispatch = useAppDispatch();

  const router = createBrowserRouter([
    {
      path: "/",
      element: <RootPage />,
      errorElement: <ErrorPage />,
      children: [
        { path: "", element: <SearchMoviePage /> },
        { path: "detail/:id", element: <DetailMoviePage /> },
        { path: "favorites", element: <FavoriteMoviesPage /> },
      ],
    },
  ]);

  /**
   * Load array of movies IDs from local storage on page load.
   */
  const loadMoviesFromStorage = () => {
    const favoriteMoviesIds = localStorage.getItem("moviesIds");

    if (favoriteMoviesIds) {
      const moviesFromStorage: string[] = JSON.parse(favoriteMoviesIds);
      dispatch(favoriteMoviesActions.saveLoadedMovies(moviesFromStorage));
    }
  };

  useEffect(() => {
    loadMoviesFromStorage();
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router} />
    </QueryClientProvider>
  );
}

export default App;
