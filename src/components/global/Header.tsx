import { AppBar, Box, LinearProgress, Toolbar } from "@mui/material";
import { useIsFetching } from "@tanstack/react-query";
import { Link, NavLink } from "react-router-dom";
import styles from "./styles/Header.module.scss";
import VideocamIcon from "@mui/icons-material/Videocam";
import StarRateIcon from "@mui/icons-material/StarRate";

const Header = () => {
  const fetching = useIsFetching();

  let progress = null;
  if (fetching) {
    progress = <LinearProgress />;
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar className={styles["header"]}>
          <Link to={""} className={styles["header__logo"]}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="40"
              height="40"
              fill="#ffffff"
              viewBox="0 0 256 256"
            >
              <path d="M224,216H183.36A103.95,103.95,0,1,0,128,232h96a8,8,0,0,0,0-16ZM40,128a88,88,0,1,1,88,88A88.1,88.1,0,0,1,40,128Zm88-24a24,24,0,1,0-24-24A24,24,0,0,0,128,104Zm0-32a8,8,0,1,1-8,8A8,8,0,0,1,128,72Zm24,104a24,24,0,1,0-24,24A24,24,0,0,0,152,176Zm-32,0a8,8,0,1,1,8,8A8,8,0,0,1,120,176Zm56-24a24,24,0,1,0-24-24A24,24,0,0,0,176,152Zm0-32a8,8,0,1,1-8,8A8,8,0,0,1,176,120ZM80,104a24,24,0,1,0,24,24A24,24,0,0,0,80,104Zm0,32a8,8,0,1,1,8-8A8,8,0,0,1,80,136Z"></path>
            </svg>
            <span>Movie Database</span>
          </Link>

          <div className={styles["header__menu"]}>
            <NavLink
              to={""}
              className={({ isActive }) =>
                isActive
                  ? `${styles["header__menu--item-active"]} ${styles["header__menu--item"]}`
                  : `${styles["header__menu--item"]}`
              }
            >
              <VideocamIcon fontSize="large" />
              Movies
            </NavLink>
            <NavLink
              to={"favorites"}
              className={({ isActive }) =>
                isActive
                  ? `${styles["header__menu--item-active"]} ${styles["header__menu--item"]}`
                  : `${styles["header__menu--item"]}`
              }
            >
              <StarRateIcon fontSize="large" />
              Favorite Movies
            </NavLink>
          </div>
        </Toolbar>
      </AppBar>
      <br />
      {progress}
    </Box>
  );
};

export default Header;
