import { Button, IconButton, TableCell, TableRow } from "@mui/material";
import { MovieDetail } from "../../models/movies/MovieDetail";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import InfoIcon from "@mui/icons-material/Info";
import ClearIcon from "@mui/icons-material/Clear";
import { useAppDispatch } from "../../store/hooks/reduxHook";
import { favoriteMoviesActions } from "../../store/slices/favoriteMoviesSlice";
import { useState } from "react";
import ConfirmDeletingDialog from "./ConfirmDeletingDialog";
import styles from "./styles/FavoriteMovieItem.module.scss";

interface IProps {
  movie: MovieDetail | null;
}

/**
 * Component for displaying favorite movie in table and for deleting movie from favorites
 * @param props
 * @returns
 */
const FavoriteMovieItem: React.FC<IProps> = (props) => {
  const { movie } = props;
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);

  /**
   * Navigate to movie detail page
   * @param movieId
   */
  const handleShowMovieDetails = (movieId: string | null) => {
    if (movieId) {
      navigate(`/detail/${movieId}`);
    } else {
      toast.error("Movie ID is NULL!");
    }
  };

  /**
   * Remove movie from favorite movies list
   * @param movie
   */
  const handleRemoveFromFavoriteMovies = (deleteRecord: boolean) => {
    setOpenConfirmDialog(false);
    if (deleteRecord && movie) {
      dispatch(favoriteMoviesActions.removeMovieFromFavorites(movie.imdbID));
      toast.success("Movie was removed successfully");
    }
  };

  return (
    <>
      <TableRow hover className={styles["row"]}>
        <TableCell>{movie?.Title}</TableCell>
        <TableCell>{movie?.Genre}</TableCell>
        <TableCell>{movie?.Released}</TableCell>
        <TableCell>{movie?.Language}</TableCell>
        <TableCell>{movie?.Country}</TableCell>
        <TableCell>
          <div className={styles["row__buttons"]}>
            <Button
              className={styles["row__buttons--detail"]}
              variant="contained"
              startIcon={<InfoIcon />}
              size="small"
              onClick={() => handleShowMovieDetails(movie?.imdbID ?? null)}
            >
              Detail
            </Button>
            <IconButton
              className={styles["row__buttons--delete"]}
              color="error"
              onClick={() => setOpenConfirmDialog(true)}
            >
              <ClearIcon />
            </IconButton>
          </div>
        </TableCell>
      </TableRow>

      <ConfirmDeletingDialog
        isOpen={openConfirmDialog}
        handleOnClose={handleRemoveFromFavoriteMovies}
      />
    </>
  );
};

export default FavoriteMovieItem;
