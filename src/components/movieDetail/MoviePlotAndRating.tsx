import { Box, Tab, Tabs } from "@mui/material";
import { useState } from "react";
import styles from "./styles/MoviePlotAndRating.module.scss";
import { MovieDetail } from "../../models/movies/MovieDetail";

interface IProps {
  movie: MovieDetail | null;
}

/**
 * Component for displaying movie plot and rating
 * @param props
 * @returns
 */
const MoviePlotAndRating: React.FC<IProps> = (props) => {
  const { movie } = props;
  const [selectedTab, setSelectedTab] = useState<number>(0);

  return (
    <div className={styles["details"]}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs className={styles["details__tabs"]} value={selectedTab}>
          <Tab label="Plot" onClick={() => setSelectedTab(0)} />
          <Tab label="Ratings" onClick={() => setSelectedTab(1)} />
        </Tabs>
      </Box>

      {selectedTab === 0 && (
        <div className={styles["details__plot"]}>
          <p>{movie?.Plot}</p>
        </div>
      )}
      {selectedTab === 1 && (
        <div className={styles["details__ratings-wrapper"]}>
          <div className={styles["details__score"]}>
            <p className={styles["details__score--text"]}>
              <span>Metascore:</span>
              {movie?.Metascore}
            </p>
            <p className={styles["details__score--text"]}>
              <span>IMDB rating:</span>
              {movie?.imdbRating}
            </p>
            <p className={styles["details__score--text"]}>
              <span>IMDB votes:</span>
              {movie?.imdbVotes}
            </p>
          </div>

          <div className={styles["details__ratings"]}>
            {movie?.Ratings.map((rating) => (
              <div
                className={styles["details__ratings-item"]}
                key={rating.Source}
              >
                <span>{rating.Source}</span>
                <span>{rating.Value}</span>
              </div>
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

export default MoviePlotAndRating;
