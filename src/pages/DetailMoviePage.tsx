import { useQuery } from "@tanstack/react-query";
import { useParams } from "react-router-dom";
import { getMovieDetailInfo } from "../util/httpRequests";
import MovieInfo from "../components/movieDetail/MovieInfo";

/**
 * Page for displaying information about specific movie
 * @returns
 */
const DetailMoviePage = () => {
  const { id } = useParams();

  /**
   * Loading movie details
   */
  const { data, isError, error } = useQuery({
    queryKey: ["movies", id],
    queryFn: () => (id ? getMovieDetailInfo(id) : null),
  });

  if (isError) {
    throw new Error(error.message);
  }

  return <MovieInfo movie={data ?? null} />;
};

export default DetailMoviePage;
