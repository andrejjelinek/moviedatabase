import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

interface FavoriteMovieState {
  favoriteMovies: string[];
}

const initialState: FavoriteMovieState = {
  favoriteMovies: [],
};

const saveMoviesToStorage = (movies: string[]) => {
  localStorage.setItem("moviesIds", JSON.stringify(movies));
};

/**
 * Slice for managing favorite movies
 */
const favoriteMoviesSlice = createSlice({
  name: "favoriteMovies",
  initialState,
  reducers: {
    addMovieToFavorites: (state, action: PayloadAction<string>) => {
      const movie = state.favoriteMovies.find(
        (movieId) => movieId === action.payload
      );
      if (movie) {
        toast.warning("This movie is already added to favorites");
      } else {
        state.favoriteMovies.push(action.payload);
        saveMoviesToStorage(state.favoriteMovies);
        toast.success("Movie was added to favorites.");
      }
    },
    removeMovieFromFavorites: (state, action: PayloadAction<string>) => {
      state.favoriteMovies = state.favoriteMovies.filter(
        (movieId) => movieId !== action.payload
      );
      saveMoviesToStorage(state.favoriteMovies);
    },
    saveLoadedMovies: (state, action: PayloadAction<string[]>) => {
      state.favoriteMovies = action.payload;
    },
  },
});

export const favoriteMoviesActions = favoriteMoviesSlice.actions;
export default favoriteMoviesSlice.reducer;
