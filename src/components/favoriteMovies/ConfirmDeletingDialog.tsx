import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import styles from "./styles/FavoriteMovieItem.module.scss";

interface IProps {
  isOpen: boolean;
  handleOnClose: (deleteRecord: boolean) => void;
}

/**
 * Component for confimation of removing movie from favorites
 * @param props
 * @returns
 */
const ConfirmDeletingDialog: React.FC<IProps> = (props) => {
  const { isOpen, handleOnClose } = props;

  return (
    <Dialog
      className={styles["confirm-dialog"]}
      open={isOpen}
      onClose={() => handleOnClose(false)}
    >
      <DialogTitle className={styles["confirm-dialog--title"]}>
        Removing favorite movie
      </DialogTitle>
      <DialogContent>
        <DialogContentText className={styles["confirm-dialog--text"]}>
          Are you sure you want to delete this movie from favorites?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          className={styles["confirm-dialog--button"]}
          variant="outlined"
          onClick={() => handleOnClose(false)}
          color="error"
        >
          Cancel
        </Button>
        <Button
          className={styles["confirm-dialog--button"]}
          variant="contained"
          onClick={() => handleOnClose(true)}
          color="success"
        >
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDeletingDialog;
