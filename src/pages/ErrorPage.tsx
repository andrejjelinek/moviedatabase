import { useRouteError } from "react-router-dom";
import styles from "./styles/ErrorPage.module.scss";
import Header from "../components/global/Header";
import errorImg from "../assets/error.svg";

/**
 * Error page occurs when something went wrong
 * @returns
 */
const ErrorPage = () => {
  const error = useRouteError() as Error;
  return (
    <>
      <Header />
      <div className={styles["error-page"]}>
        <p className={styles["error-page--title"]}>
          OOPS! Something went wrong
        </p>
        <p className={styles["error-page--name"]}>{error.name}</p>
        <p className={styles["error-page--message"]}>{error.message}</p>
        <img src={errorImg} />
      </div>
    </>
  );
};

export default ErrorPage;
