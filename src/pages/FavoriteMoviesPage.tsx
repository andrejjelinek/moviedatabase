import { useState } from "react";
import { useAppSelector } from "../store/hooks/reduxHook";
import { useQuery } from "@tanstack/react-query";
import { getMovieDetailInfo } from "../util/httpRequests";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from "@mui/material";
import FavoriteMovieItem from "../components/favoriteMovies/FavoriteMovieItem";
import styles from "./styles/FavoriteMoviesPage.module.scss";

/**
 * Columns for table of favorite movies
 */
const TABLE_COLUMNS: { id: number; column: string }[] = [
  { id: 0, column: "Title" },
  { id: 1, column: "Genre" },
  { id: 2, column: "Released" },
  { id: 3, column: "Language" },
  { id: 4, column: "Country" },
  { id: 5, column: "" },
];

/**
 * Page for displaying table with our favorite movies
 * @returns
 */
const FavoriteMoviesPage = () => {
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);

  /**
   * Favorite movies from redux store
   */
  const favoriteMovies = useAppSelector(
    (state) => state.favoriteMovies.favoriteMovies
  );

  /**
   * Loading favorite movies
   */
  const { data, isError, error } = useQuery({
    queryKey: ["favoriteMovies", favoriteMovies, page, rowsPerPage],
    queryFn: () => loadMoviesDetails(),
  });

  /**
   * Send requests to load favorite movies details for selected page and rows per page
   * @returns MovieDetail[]
   */
  const loadMoviesDetails = async () => {
    const moviesPromises = [];
    const maxIndex = page * rowsPerPage + rowsPerPage;

    for (let i = page * rowsPerPage; i < maxIndex; i++) {
      if (i < favoriteMovies.length) {
        moviesPromises.push(getMovieDetailInfo(favoriteMovies[i]));
      }
    }

    const moviesDetails = await Promise.all(moviesPromises);
    return moviesDetails.filter((movie) => movie !== null);
  };

  /**
   * Change selected page
   * @param event
   * @param newPage
   */
  const handleChangePage = (_event: unknown, newPage: number) => {
    setPage(newPage);
  };

  /**
   * Change rows per page
   * @param event
   */
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  if (isError) {
    throw new Error(error.message);
  }

  return (
    <section className={styles["favorite-movies"]}>
      <p>Favorite movies</p>

      <TableContainer className={styles["favorite-movies__table"]}>
        <Table size="small">
          <TableHead>
            <TableRow className={styles["favorite-movies__table-row"]}>
              {TABLE_COLUMNS.map((row) => (
                <TableCell key={row.id} style={{ minWidth: "100px" }}>
                  {row.column}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data?.map((row) => (
              <FavoriteMovieItem key={row?.imdbID} movie={row} />
            ))}
          </TableBody>
        </Table>
        <TablePagination
          className={styles["favorite-movies__pagination"]}
          rowsPerPageOptions={[5, 10, 20]}
          component="div"
          count={favoriteMovies.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </TableContainer>
    </section>
  );
};

export default FavoriteMoviesPage;
